# Applications

Brainstorming ideas for possible applications of using SDO data at scale

* Time lags from many ARs over long periods of time
* DEMs from many ARs
  * At each time step, not just time-average
* Surveying heating properties from many ARs (related to first two points)
* How is the Poynting flux correlated with thermal structure of the AR?
  * Use of HMI vector magnetograms to get E-field
  * Combined with thermal diagnostics from AIA
* Running ratio/difference movies of EUV waves from full-disk images
* Isolating Fe 18 from 94 AIA channel
* Event detection in isolated Fe 18 over whole image and time
    * Method of Ugarte-Urra et al. (2014)
    * Method of Plowmann
    * Method of Graham et al. (2019)


## Notebooks

More fully-formed ideas for self-contained workflow examples. Lots of overlap with above.
As these become more polished examples, they should go in `notebooks/tidy`

* Querying Data with drms
  * Basic example of how to query data
  * Note integration with existing library with minimal code
  * Future: Integration with SunPy Fido client
* Basic Batch Processing and Data Structures
  * Basics of Dask and dask-jobqueue
  * Show how to map function over many files to produce derotated level 1.5 data
  * Constuct AIACube and show basic functionality of NDCube
  * Selecting regions of interest, slicing in time all with existing code, integration with astropy/sunpy/PyHC ecosystem
  * Construct AIASequence for holding many wavelengths
* Running ratio analysis of the EUV wave from the Sept. 10, 2017 flare (see Liu et al., 2018)
  * Full-disk
  * ~2 hrs of data
  * 3 channels: 171, 193, 211
* Time lag analysis of non-flaring ARs (see Viall and Klimchuk, 2012-2017)
  * Cutout around AR
  * ~12 hrs of data
  * All 6 EUV channels
  * Start with a single AR from Warren et al. (2012), but can expand to more
  * e.g. Barnes et al. (2019)
* Heating event detection in isolated Fe XVIII emission (see Ugarte-Urra et al., 2018)
  * Cutout around AR
  * 6 hrs of data
  * 3 channels: 94, 171, 193
* Sunspot detection in HMI/MDI continuum images with the STARA algorithm (see [Fraser et al., 2009](https://doi.org/10.1007/s11207-009-9420-z))
  * Full-disk ?
  * Many, i.e. ~lifetime of the instrument
  * 1 channel
  * Detection is on individual images
  * Only a crude Python implementation; see [here](https://github.com/Cadair/sunspot_experiments)
* Calculating SHARP Features from Vector Magnetogram Data
  * Can HPC plus high data availability be used to calculate SHARP features very quickly?
  * Good proof-of-concept for those who want to reprocess vector magnetogram data
  * Could be very useful for deep learning applications (e.g. flare prediction)
  * Use `hmi.sharp_cea_720s` which holds predefined cutouts with `B_r`,`B_p`,`B_t` calculated in CEA projection
  * Need a list of HARP numbers and `T_REC` keywords to iterate over
  * See [github.com/mbobra/calculating-spaceweather-keywords](https://github.com/mbobra/calculating-spaceweather-keywords) for implementation of SHARP calculations in Python

